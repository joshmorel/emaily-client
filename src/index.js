// data layer control (Redux)
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';

import 'materialize-css/dist/css/materialize.min.css'; //looks in node_modules if path not relative

import App from './components/App';
import reducers from './reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Register all reducers here!
// Initial state will always be empty object as no server-side rendering.
const store = createStore(reducers, {}, composeEnhancers(
  applyMiddleware(reduxThunk)
));

ReactDOM.render(
  <Provider store={store}><App/></Provider>,
  document.getElementById('root')
);

// console.log("stripe key is ", process.env.REACT_APP_STRIPE_KEY);
// console.log('environment is',process.env.NODE_ENV);
