import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import authReducer from './authReducer';
import surveyReducer from './surveyReducer';

export default combineReducers({
  // auth piece of state being processed by authReducer
  auth: authReducer,
  surveys: surveyReducer,
  form: reduxForm
});
