import {FETCH_SURVEYS} from "../actions/types";

//null - by default have no clue if user is logged in
export default function(state = [], action) {
  switch (action.type) {
    case FETCH_SURVEYS:
      return action.payload;
    default:
      return state;
  }
}
