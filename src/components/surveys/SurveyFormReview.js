import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import formFields from './formFields';
import * as actions from '../../actions';

const SurveyFormReview = ({formValues, submitSurvey, onCancel, history}) => {

  const renderFields = () => {
    return formFields.map(({ name, label}) => {
      return (
        <div key={name}>
          <label>{label}</label>
          <div>{formValues[name]}</div>
        </div>
      );
    })
  }
  ;

  return (
    <div>
      <h5>Please confirm your entries</h5>
      {renderFields()}
      <button className="yellow darken-3 white-text btn-flat" onClick={onCancel}>
        Back
      </button>
      <button
        className="green btn-flat right white-text"
        onClick={() => submitSurvey(formValues, history)}
      >

        Send Survey
        <i className="material-icons right">email</i>
      </button>

    </div>
  );
};
// const SurveyField = ({input, label, meta: {error, touched}}) => {
//   return (
function mapStateToProps(state) {
  return {formValues: state.form.surveyForm.values };
}

export default connect(mapStateToProps, actions)(withRouter(SurveyFormReview));
